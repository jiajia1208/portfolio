package OOMTester;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.rosuda.JRI.*;

import HBase.Helper.HBaseHelper;
import main.StateTaRuleMap;

public class OOMTester {

	HBaseHelper hb = HBaseHelper.getHBaseHelperInstance();
	private static StateTaRuleMap a = new StateTaRuleMap();
	private static Map<String, Integer> s = a.getMap();

	public static void main(String[] args) throws IOException, ParseException {

		OOMTester CSVFileCreater1 = new OOMTester();
		RVector StateTag = new RVector();
		for (Map.Entry<String, Integer> entry : s.entrySet()) {
			StateTag.addElement(entry.getValue().toString());
		}

		CSVFileCreater1.createCSVFile("taiwan", "future", "TXK5", "1s",
				"20150414090000", "20150508140000", StateTag);

	}

	public List<List<String>> createCSVFile(String Region, String Market,
			String SymbolID, String TimeGranularity, String BeginTime,
			String EndTime, RVector StateNo) throws IOException, ParseException {

		List<List<String>> result = new ArrayList<List<String>>();

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMddhhmmss");
		Calendar beginTimeCalendar = Calendar.getInstance();
		beginTimeCalendar.setTime(dateFormat1.parse(BeginTime));
		Calendar endTimeCalendar = Calendar.getInstance();
		endTimeCalendar.setTime(dateFormat1.parse(EndTime));

		String rowKey = "";
		int lineNo = 0;
		int noneEmptyNo = 0;
		int emptyNo = 0;
		String cellResult = "";
		int biggestString = 0;

		while (beginTimeCalendar.compareTo(endTimeCalendar) != 1) {

			rowKey = Region + "/" + Market + "/" + SymbolID + "/"
					+ TimeGranularity + "/"
					+ dateFormat1.format(beginTimeCalendar.getTime());
			Result rowResult = hb.getRow(Bytes.toBytes(rowKey), "Stock");

			if (rowResult.size() != 0) {
				List<String> subResult = new ArrayList<String>();

				for (int i = 1; i <= StateNo.size(); i++) {

					if (rowResult.getValue(Bytes.toBytes("State"),
							Bytes.toBytes(StateNo.get(i - 1).toString())) != null) {
						cellResult = Bytes.toString(rowResult.getValue(
								Bytes.toBytes("State"),
								Bytes.toBytes(StateNo.get(i - 1).toString())));
						subResult.add(cellResult);

						if (cellResult.length() > biggestString)
							biggestString = cellResult.length();
						noneEmptyNo++;
					} else {
						subResult.add(" ");
						emptyNo++;
					}
				}
				result.add(subResult);
				lineNo++;
				System.out.println("Data Size :" + lineNo + "x"
						+ StateNo.size() + " ,noneEmptyNo :" + noneEmptyNo
						+ " ,emptyNo :" + emptyNo + " ,The Longgest String :"
						+ biggestString);
				noneEmptyNo = 0;
				emptyNo = 0;

			}
			beginTimeCalendar.add(Calendar.SECOND, 1);
		}

		return result;
	}
}
