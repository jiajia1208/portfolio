package CSVFileCreater;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.rosuda.JRI.*;

import HBase.Helper.HBaseHelper;
import main.StateTaRuleMap;

public class CSVFileCreater {
	
	HBaseHelper hb = HBaseHelper.getHBaseHelperInstance();
	private static StateTaRuleMap a = new StateTaRuleMap();
	private static Map<String, Integer> s = a.getMap();

	public static void main(String[] args) throws IOException, ParseException {

		CSVFileCreater CSVFileCreater1 = new CSVFileCreater();

		RVector StateTag = new RVector();
		for (Map.Entry<String, Integer> entry : s.entrySet()) {
			System.out.print(entry.getValue().toString() + ",");
			StateTag.addElement(entry.getValue().toString());
		}
		System.out.print(CSVFileCreater1.createCSVFile("taiwan", "future",
				"TXK5", "1s", "20150414090000", "20150422140000", StateTag));

	}

	public String createCSVFile(String Region, String Market, String SymbolID,
			String TimeGranularity, String BeginTime, String EndTime,
			RVector StateNo) throws IOException, ParseException {

		Random ran = new Random();
		String CSVFilePath = System.getProperty("user.dir") + "/"
				+ (ran.nextInt(9999) + 1) + ".csv";

		SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyyMMddhhmmss");
		SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
		Calendar beginTimeCalendar = Calendar.getInstance();
		beginTimeCalendar.setTime(dateFormat1.parse(BeginTime));
		Calendar endTimeCalendar = Calendar.getInstance();
		endTimeCalendar.setTime(dateFormat1.parse(EndTime));

		File CSV = new File(CSVFilePath);
		BufferedWriter CSVWriter = new BufferedWriter(
				new FileWriter(CSV, false));

		CSVWriter.write("DateTime");
		for (int i = 1; i <= StateNo.size(); i++) {
			CSVWriter.write("," + StateNo.get(i - 1));
		}
		CSVWriter.newLine();

		String rowKey = "";

		while (beginTimeCalendar.compareTo(endTimeCalendar) != 1) {

			rowKey = Region + "/" + Market + "/" + SymbolID + "/"
					+ TimeGranularity + "/"
					+ dateFormat1.format(beginTimeCalendar.getTime());
			Result rowResult = hb.getRow(Bytes.toBytes(rowKey), "Stock");

			if (!rowResult.isEmpty()) {
				CSVWriter
						.write(dateFormat1.format(beginTimeCalendar.getTime()));

				for (int i = 1; i <= StateNo.size(); i++) {

					if (rowResult.getValue(Bytes.toBytes("State"),
							Bytes.toBytes(StateNo.get(i - 1).toString())) != null) {
						CSVWriter
								.write(","
										+ Bytes.toString(rowResult.getValue(
												Bytes.toBytes("State"), Bytes
														.toBytes(StateNo.get(
																i - 1)
																.toString()))));
					} else {
						CSVWriter.write(", ");
					}
				}
				CSVWriter.newLine();
			}
			beginTimeCalendar.add(Calendar.SECOND, 1);
		}

		System.out.print(CSVFilePath);

		return CSVFilePath;
	}
}
