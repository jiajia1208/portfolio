package HDFS.Builder

import HBase.Helper.HBaseHelperS
import HDFS.Helper.HDFSHelper
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import scala.util.Random
import java.io._

class HDFSBuilder(Region: String, Market: String, SymbolID: String, TimeGranularity: String, StateTag: List[String], DateTimeList: List[String], Label: List[Int], Folder: String) {

  val vMatrixID = Region.substring(0, 1) + Market.substring(0, 1) + SymbolID.substring(0, 1) + TimeGranularity.substring(0, 1) + "_" + getRandomInt()
  var mSize = StateTag.length // num of column
  var nSize = 0 // num of row

  val hb = HBaseHelperS

  def createHDFSFile(): String = {
    //vertical rowkey region/market/SymbolID/TG/time
    //horizontal rawkey region/market/SymbolID/TG/date/Tag/SegNo

    var rowkeyIndex = 1

    try {
      var tempResultFile = new File("/home/kmlab/HDFSFiles/" + Folder + "/" + vMatrixID + ".txt")
      tempResultFile.getParentFile().mkdirs();
      var tempResultFileWriter = new BufferedWriter(new FileWriter(tempResultFile, false))

      for (i <- 0 to DateTimeList.length - 1) {

        var rowList: List[Double] = List()
        var rowkey = Region + "/" + Market + "/" + SymbolID + "/" + TimeGranularity + "/" + DateTimeList(i)
        var rowResult = hb.getRow(Bytes.toBytes(rowkey), "Stock") // get Row Data

        //      var VMRatioRowkey = vMatrixID + "/" + rowResult.getValue(Bytes.toBytes("State"), Bytes.toBytes(Ratio)) + "/" + getRowNumString(rowkeyIndex)

        if (rowResult.size() != 0) {

          tempResultFileWriter.write(Label(i).toString) //write Label
          for (k <- 0 to StateTag.length - 1) {

            var tempStateValue = rowResult.getValue(Bytes.toBytes("State"), Bytes.toBytes(StateTag(k)))

            if (tempStateValue == null || tempStateValue == Bytes.toBytes("NaN") || tempStateValue == Bytes.toBytes("")) {
              tempResultFileWriter.write(" " + (k + 1).toString + ":0")
            } else if (tempStateValue != Bytes.toBytes("Infinity")) {
              tempResultFileWriter.write(" " + (k + 1).toString + ":999999")
            } else if (tempStateValue != Bytes.toBytes("-Infinity")) {
              tempResultFileWriter.write(" " + (k + 1).toString + ":-999999")
            } else {
              tempResultFileWriter.write(" " + (k + 1).toString + ":" + Bytes.toString(tempStateValue))
            }
          }
          //      hb.putVMRatio(VMRatioPut)
          tempResultFileWriter.newLine()
        }

      }
      tempResultFileWriter.close()
    } catch {
      case e: IOException => e.printStackTrace()
    }

    val HDFSHelper = new HDFSHelper()

    val localPath = "/home/kmlab/HDFSFiles/" + Folder + "/" + vMatrixID + ".txt"
    val serverPath = "hdfs://nccu-master:8020/home/kmlab/VMatrix/" + Folder

    HDFSHelper.createDirectoryOnHDFS(serverPath)
    HDFSHelper.HDFSUploadFile(localPath, serverPath)

    var filePath = "hdfs://nccu-master:8020/home/kmlab/VMatrix/" + Folder + "/" + vMatrixID + ".txt"

    filePath
  }

  def getRandomInt(): String = {
    var num = Random.nextInt(99999999)
    var result = num.toString
    if (num < 10000000) {
      result = "0" + result
    }
    if (num < 1000000) {
      result = "0" + result
    }
    if (num < 100000) {
      result = "0" + result
    }
    if (num < 10000) {
      result = "0" + result
    }
    if (num < 1000) {
      result = "0" + result
    }
    if (num < 100) {
      result = "0" + result
    }
    if (num < 10) {
      result = "0" + result
    }

    result
  }

}