package HDFS.Helper;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSHelper {
	public Configuration conf = new Configuration();

	// conf.set("fs.default.name", "hdfs://nccu-master:8020");
	// conf.set("fs.hdfs.impl", value);
	// conf.set("mapred.jop.tracker", "hdfs://nccu-master:8021");

	public void HDFSHelper() {
		conf.addResource(new Path(
				"hdfs://nccu-master:8020/etc/hadoop/conf/core-site.xml"));
		conf.addResource(new Path(
				"hdfs://nccu-master:8020/etc/hadoop/conf/hdfs-site.xml"));
		conf.addResource(new Path(
				"hdfs://nccu-master:8020/etc/hadoop/conf/mapred-site.xml"));

	}

	public void HDFSUploadFile(String clientPath, String ServerPath) {

		Path src = new Path(clientPath);
		// 目的位置
		Path dst = new Path(ServerPath);
		try {
			FileSystem fs = FileSystem.get(conf);

			fs.copyFromLocalFile(src, dst);

			fs.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void createDirectoryOnHDFS(String path) throws Exception {

		FileSystem fs = FileSystem.get(conf);
		Path dst = new Path(path);
		fs.mkdirs(dst);
		fs.close();// 释放资源

	}
	public void deleteFileOnHDFS(String serverFilePath) {

		try {
			FileSystem fileSystem = FileSystem.get(conf);
			Path path = new Path(serverFilePath);
			fileSystem.deleteOnExit(path);
			fileSystem.close();// 释放资源

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
