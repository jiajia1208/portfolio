package Pre.Processing;

import Miner.MinerConfig

object PreProcManager {

  var dataSet: List[String] = List()
  var sigmaConfirm = false

  def excute(minerConfig: MinerConfig):List[String]= {
    val dataSetBuilder = new DataSetBuilder(minerConfig)
    //targetList.foreach(println)
    
    val preProcType = minerConfig.preProcType
    
    if (preProcType == "RandomSubSet") {
      dataSet = dataSetBuilder.getRandomSubset
    }else if(preProcType == "RatioSubSet"){
      dataSet = dataSetBuilder.getRatioSubset
    }else if(preProcType == "RatioRanSubSet"){
      dataSet = dataSetBuilder.getRatioRanSubset
    }else{
      println("Wrong PreProcessing Type!!!")
    }

    dataSet
  }
}